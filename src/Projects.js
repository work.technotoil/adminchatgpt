/* eslint-disable arrow-body-style */
/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react';
import {
  collection,
  query,
  doc,
  onSnapshot,
  setDoc,
  updateDoc,
  deleteDoc,
  getDoc,
  getDocFromCache,
} from 'firebase/firestore';
import { db } from './firebase';
import { addDbData } from './utils/CrudFunction';

function Projects() {
  const [tasks, setTasks] = useState([]);
  const [tasksId, setTasksId] = useState([]);
  const [domain, setDomain] = useState([]);

  useEffect(() => {
    const q = query(collection(db, 'company'));
    onSnapshot(q, (querySnapshot) => {
      setTasks(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        }))
      );
    });
  }, []);

  const addData = async () => {
    const array = [
      {
        id: 4,
        name: `dharmesh4`,
      },
      {
        id: 2,
        name: `dharmesh2`,
      },
      {
        id: 3,
        name: `dharmesh3`,
      },
    ];
    // console.log('tasks', tasks);
    // const main = await getDoc(doc(db, 'company', '5'));

    // console.log('main', main);

    // get data by id
    const docRef = doc(db, 'company', '5');
    const doc2 = await getDocFromCache(docRef);
    console.log('docdocdoc', doc2.data());

    // get all data
    const domain = query(collection(db, 'domain'));
    onSnapshot(domain, (querySnapshot) => {
      setDomain(querySnapshot.docs.map((doc) => doc.data()));
    });

    // const docData = {
    //   id: tasks.length + 1,
    //   name: `dharmesh${tasks.length + 1}`,
    // };

    // add single data in firebase db
    // await setDoc(doc(db, 'company', (tasks.length + 1).toString()), docData);

    // add multiple data in firebase db
    // array.forEach(async (doc) => {
    //   await addDbData('company', `collection_${doc.id}`, doc);
    // });

    // update data in firebase db
    // const docRef = doc(db, 'company', '1');
    // const data = {
    //   name: 'kajhal jhaaa',
    // };
    // await updateDoc(docRef, data);

    // delete data from firebase db
    // const deleteRef2 = doc(db, 'company', (tasks.length - 2).toString());
    // await deleteDoc(deleteRef2);
  };

  console.log('domain', domain);

  return (
    <div>
      <p>hello</p>
      {tasks.map((project, index) => {
        return <p key={index}>{project.data.name}</p>;
      })}
      <button onClick={addData}>add</button>
    </div>
  );
}

export default Projects;
