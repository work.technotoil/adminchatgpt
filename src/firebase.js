// // import { initializeApp } from 'firebase/app';
// // import { getDatabase } from 'firebase/database';

// // const firebaseConfig = {
// //   apiKey: 'AIzaSyCsTrmLAti981AB67wngR2TMp0MwhkDbF8',
// //   authDomain: 'conpamy-admin1.firebaseapp.com',
// //   projectId: 'conpamy-admin1',
// //   storageBucket: 'conpamy-admin1.appspot.com',
// //   messagingSenderId: '574029070307',
// //   appId: '1:574029070307:web:cc0e66df62db1311b5ead6',
// //   measurementId: 'G-W5TKMJGD9Q',
// // };

// // // Initialize Firebase
// // const app = initializeApp(firebaseConfig);
// // // const analytics = getAnalytics(app);
// // export const db = getDatabase(app);

import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCsTrmLAti981AB67wngR2TMp0MwhkDbF8',
  authDomain: 'conpamy-admin1.firebaseapp.com',
  projectId: 'conpamy-admin1',
  storageBucket: 'conpamy-admin1.appspot.com',
  messagingSenderId: '574029070307',
  appId: '1:574029070307:web:cc0e66df62db1311b5ead6',
  measurementId: 'G-W5TKMJGD9Q',
};

// Initialize Firebase and Firestore
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
export { db };
