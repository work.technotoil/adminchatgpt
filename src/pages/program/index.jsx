import React, { useEffect, useState } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import {
    collection,
    query,
    doc,
    onSnapshot,
    setDoc,
    updateDoc,
    deleteDoc,
    getDoc,
    getDocFromCache,
} from 'firebase/firestore';

import InputBox from '../../components/inputBox';
import AdminModal from '../../components/modal/index';
import AdminTable from '../../components/adminTable';
import { db } from '../../firebase';

const Program = () => {

    const [open, setOpen] = useState(false)
    const [programValue, setProgramValue] = useState("")
    const [program, setProgram] = useState([])
    const openClick = () => {
        setOpen(true)
    }

    useEffect(() => {
        const q = query(collection(db, 'company'));
        onSnapshot(q, (querySnapshot) => {
            setProgram(
                querySnapshot.docs.map((doc) => (
                    doc.data()
                ))
            );
        });
    }, []);
    const handleClose = () => {
        setOpen(false)
    }
    const programChange = (data) => {
        setProgramValue(data)
    }


    const tableHead = <TableHead>
        <TableRow>
            <TableCell>Id</TableCell>
            <TableCell>Name</TableCell>
        </TableRow>
    </TableHead>
    return (
        <div>
            <AdminTable {...{ tableHead, openClick }} headingText="Program" btnText="Add Program">
                <TableBody>
                    {program.map((row) => (
                        <TableRow
                            key={row.name}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell>{row.name}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </AdminTable>
            <AdminModal {...{ open, handleClose }} >
                <InputBox onChange={programChange} type="program" label="Program" name="program" value={programValue} placeholder="Enter program" />
            </AdminModal>
        </div>
    )
}

export default Program