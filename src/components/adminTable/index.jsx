/* eslint-disable react/prop-types */
/* eslint-disable arrow-body-style */
import React from 'react'
import Paper from '@mui/material/Paper';
import { Box, Button, Table, TableContainer, TablePagination, Typography } from '@mui/material';
import AdminButton from '../adminButton';
import "./index.css"

const AdminTable = ({ tableHead, children, openClick, btnText, headingText, rows, rowsPerPage, page, handleChangePage, handleChangeRowsPerPage }) => {
    return (
        <TableContainer component={Paper} x={{ maxHeight: 440 }}>
            <Box className="tableHeader">
                <Typography variant='h4'>
                    {headingText}
                </Typography>
                <AdminButton {...{ openClick, btnText }} />
            </Box>
            <Table stickyHeader sx={{ minWidth: 650 }} aria-label="simple table">
                {tableHead}
                {children}
            </Table>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </TableContainer>
    )
}

export default AdminTable