/* eslint-disable react/prop-types */
/* eslint-disable arrow-body-style */
import { TextField } from '@mui/material'
import React from 'react'

const InputBox = ({ label, name, value, placeholder, onChange, type }) => {
    return (
        <TextField
            label={label}
            type="text"
            onChange={(e) => onChange(e.target.value, type)}
            placeholder={placeholder}
            name={name}
            value={value}
            autoComplete="current-password"
        />
    )
}

export default InputBox