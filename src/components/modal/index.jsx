/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
import React from 'react'
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import { Cancel } from '../../utils/imagePath';
import "./index.css"

const AdminModal = ({ handleClose, open, children }) => {
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    return (
        <Modal
            open={open}
            disableEscapeKeyDown
            onClose={handleClose}
            className='adminModal'
        >
            <Box sx={style}>
                <img src={Cancel} alt="" onClick={handleClose} />
                {children}
            </Box>
        </Modal>
    )
}

export default AdminModal   