/* eslint-disable react/prop-types */
/* eslint-disable arrow-body-style */
import { Button } from '@mui/material'
import React from 'react'

const AdminButton = ({ openClick, btnText }) => {
    return (
        <Button className='admin_button' onClick={openClick} variant="contained" size="medium">
            {btnText}
        </Button>
    )
}

export default AdminButton