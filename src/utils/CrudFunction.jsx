import { collection, query, doc, onSnapshot, setDoc, updateDoc, deleteDoc } from 'firebase/firestore';
import { db } from "../firebase";

export const addDbData = async (tableName, id, data) => {
    await setDoc(doc(db, tableName, id.toString()), data)
}